import { LightningElement, track } from 'lwc';
import add from '@salesforce/apex/Comp1Controller.add';
import sub from '@salesforce/apex/Comp1Controller.sub';
import mul from '@salesforce/apex/Comp1Controller.mul';
import div from '@salesforce/apex/Comp1Controller.div';

export default class Comp1 extends LightningElement {


    a;
    b;
    value;
    ifchecked;
    @track
    history=[];

    inputhandler(event){
        if(event.target.name==='first'){
            this.a=event.target.value;
        }else if(event.target.name==='second'){
            this.b=event.target.value;
        }
    }

    checkboxhandler(event){
        this.ifchecked=event.target.checked;
    }

    addhandler(){
    add({f:this.a,s:this.b}).then(res=>{
        this.value=res;
        const ob=`Result of ${this.a} + ${this.b} = ${res}`;
            this.history.push(ob);
            console.log(this.history);
    }).catch(error=>{
    });
}
divhandler(){
    div({f:this.a,s:this.b}).then(res=>{
        this.value=res;
        const ob=`Result of ${this.a} / ${this.b} = ${res}`;
            this.history.push(ob);
    }).catch(error=>{
        console.log(error.body.message);
    });
}
    
    subhandler(){
        sub({f:this.a,s:this.b}).then(res=>{
            this.value=res;
            const ob=`Result of ${this.a} - ${this.b} = ${res}`;
            this.history.push(ob);
        }).catch(error=>{
            console.log(error.body.message);
        });
    }
   

    mulhandler(){
        mul({f:this.a,s:this.b}).then(res=>{
            this.value=res;
            const ob=`Result of ${this.a} x ${this.b} = ${res}`;
            this.history.push(ob);
        }).catch(error=>{
            console.log(error.body.message);
        });
    }
}
