public with sharing class Comp1Controller{

    public class Comp1ControllerException extends Exception{}
    
    @AuraEnabled(cacheable=true)
    public static decimal add(decimal f,decimal s){
      return f+s;
    }

    @AuraEnabled(cacheable=true)
    public static decimal sub(decimal f,decimal s){
      return f-s;
    }

    @AuraEnabled(cacheable=true)
    public static decimal mul(decimal f,decimal s){
        if(f==0||s==0){
            throw new Comp1ControllerException('Doesnt make sense to multiply by 0');
        }
      return f*s;
    }

    @AuraEnabled(cacheable=true)
    public static decimal div(decimal f,decimal s){
        if(s==0){
            throw new Comp1ControllerException('denominator cant be 0');
        }
      return f/s;
    }
}
